import { LikeActionType } from '../../enums/like';

export type LikeSection = 'MOVIES' | 'TV_SHOWS'

export type LikeState = {
  MOVIES: []
  TV_SHOWS: []
}

/* --------------------------------- ACTIONS -------------------------------- */

export type SetLike = { 
  payload: { id: number, section: LikeSection }, 
  type: LikeActionType.LIKE 
}

export type SetDislike = { 
    payload: { id: number, section: LikeSection }, 
    type: LikeActionType.DISLIKE 
}