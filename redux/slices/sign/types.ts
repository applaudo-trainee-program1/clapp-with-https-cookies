import { SignActionType } from '../../enums/sign';

export type SignState = {
  isLogged: boolean
}

/* --------------------------------- ACTIONS -------------------------------- */

export type SetSignInAction = { type: SignActionType.SIGN_IN }

export type SetSignOutAction = { type: SignActionType.SIGN_OUT }