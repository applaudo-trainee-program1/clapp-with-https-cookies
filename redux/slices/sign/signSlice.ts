import { createSlice } from '@reduxjs/toolkit';
import { initialSignState } from '../../../config/initials';

export const signSlice = createSlice({
  name: 'sign',
  initialState: initialSignState,
  reducers: {
    setSignOut: () => { return initialSignState;},
    setSignIn: () => { return { isLogged:true };},
  }
});

// Action creators are generated for each case reducer function
export const { setSignOut, setSignIn} = signSlice.actions;

export default signSlice.reducer;
