/* eslint-disable no-unused-vars */
export enum SignActionType {
  SIGN_IN = 'SIGN_IN',
  SIGN_OUT = 'SIGN_OUT',
}