import { mainMoviesApi, mainShowsApi } from '../apiInstances';
import genericAxiosInstance from '../apiInstances/genericApi';
import mainMoviesLocalApi from '../apiInstances/mainMoviesLocalApi';
import mainShowsLocalApi from '../apiInstances/mainShowsLocalApi';
import searchMoviesTvShowsLocalApi from '../apiInstances/searchMoviesTvShowsLocalApi';
import { CardProps } from '../components';
import { DEFAULT_PAGE_SIZE, IMAGE_BASE_URL } from '../config';
import { logo } from '../public/images';
import { Routes } from '../routes/routes';
import { GREAT_SCORE_BACKGROUND_CLASS, LOW_SCORE_BACKGROUND_CLASS, 
  MEDIUM_SCORE_BACKGROUND_CLASS, NO_SCORE_BACKGROUND_CLASS } from './constants';
import enviromentVariables from './enviromentVariables';
import { GetLocalSearchRequestConfigProps, GetMainRequestConfig, 
  getGenericDetailsRequestConfigProps, NormalizeLocalResponse, 
  NormalizeLocalSearchResponseProps, NormalizeMovieDetailsProps,
  NormalizeTvShowDetailsProps, NormalizeSeasonDetailsProps,
  GetSeasonDetailsRequestConfigProps, GetPersonDetailsRequestConfigProps,
  NormalizePersonDetailsProps} from './types';

/**
 * Get an axios request config to use with /api/main/movies endpoint
 *
 * @param {GetMainRequestConfig} { 
 *   page, 
 *   genres, 
 *   year, 
 *   certification 
 * }
 * - page: page's movies results
 * - genres: genres's movies results
 * - year: year of movie release
 * - certification: movie's US certification
 * @return {{api, requestConfig}} -
 * - api: Axios instance for "discover movies" endpoint
 * - requestConfig: Axios request config for "discover movies" endpoint
 */
export const getMainMoviesRequestConfig = ({ 
  page, 
  genres, 
  year, 
  certification 
}:GetMainRequestConfig) => {
  const { apiKey } = enviromentVariables;
  return {
    api: mainMoviesApi,
    requestConfig: {
      method: 'get',
      params: {
        api_key: apiKey,
        sort_by: 'popularity.desc',
        include_adult: 'false',
        page: page ? String(page) : '1',
        language: 'en-US',
        ...(year && {primary_release_year: String(year)}),
        ...(genres && Array.isArray(genres) && {with_genres: genres.join('|')}),
        ...(certification && {certification_country: 'US', certification}),
      },
    }
  };
};

/**
 * Get an axios request config to use with "discover movies" endpoint
 *
 * @param {GetMainRequestConfig} { 
 *   page, 
 *   genres, 
 *   year, 
 *   certification 
 * }
 * - page: page's movies results
 * - genres: genres's movies results
 * - year: year of movie release
 * - certification: movie's US certification
 * @return {{api, requestConfig}} -
 * - api: Axios instance for "discover movies" endpoint
 * - requestConfig: Axios request config for "discover movies" endpoint
 */
export const getMainMoviesLocalRequestConfig = ({ 
  page, 
  genres, 
  year, 
  certification 
}:GetMainRequestConfig) => {
  return {
    api: mainMoviesLocalApi,
    requestConfig: {
      method: 'get',
      params: {
        ...(year && {year}),
        ...(page && {page}),
        ...(genres && {genres}),
        ...(certification && {certification}),
      },
    }
  };
};

/**
 * Calculate total number of pages available
 *
 * @param {number} totalResults - Total results fetched
 * @return {number} TotalPages - Total pages available
 */
export const calculateTotalPages = (totalResults: number): number => {
  return Math.trunc((totalResults - 1) / DEFAULT_PAGE_SIZE) + 1;
};

/**
 * Get an axios request config to use with /api/main/movies endpoint
 *
 * @param {GetMainRequestConfig} { 
 *   page, 
 *   genres, 
 *   year, 
 *   certification 
 * }
 * - page: page's movies results
 * - genres: genres's movies results
 * - year: year of movie release
 * - certification: movie's US certification
 * @return {{api, requestConfig}} -
 * - api: Axios instance for "discover movies" endpoint
 * - requestConfig: Axios request config for "discover movies" endpoint
 */
export const getMainShowsRequestConfig = ({ 
  page, 
  genres, 
  year
}:GetMainRequestConfig) => {
  const { apiKey } = enviromentVariables;
  return {
    api: mainShowsApi,
    requestConfig: {
      method: 'get',
      params: {
        api_key: apiKey,
        sort_by: 'popularity.desc',
        include_adult: 'false',
        page: page ? String(page) : '1',
        language: 'en-US',
        ...(year && {first_air_date_year: String(year)}),
        ...(genres && Array.isArray(genres) && {with_genres: genres.join('|')})
      },
    }
  };
};

/**
 * Get an axios request config to use with "discover movies" endpoint
 *
 * @param {GetMainRequestConfig} { 
 *   page, 
 *   genres, 
 *   year, 
 *   certification 
 * }
 * - page: page's movies results
 * - genres: genres's movies results
 * - year: year of movie release
 * - certification: movie's US certification
 * @return {{api, requestConfig}} -
 * - api: Axios instance for "discover movies" endpoint
 * - requestConfig: Axios request config for "discover movies" endpoint
 */
export const getMainShowsLocalRequestConfig = ({ 
  page, 
  genres, 
  year
}:GetMainRequestConfig) => {
  return {
    api: mainShowsLocalApi,
    requestConfig: {
      method: 'get',
      params: {
        ...(year && {year}),
        ...(page && {page}),
        ...(genres && {genres})
      },
    }
  };
};

export type GetLocalApiProps = {
  section: GetMainRequestConfig['section']
}

export type GetLocalRequestConfigProps = {
  section: GetMainRequestConfig['section']
}

const getLocalApi = ({section}:GetLocalApiProps) => {
  switch(section){
  case 'MOVIES': return mainMoviesLocalApi;
  case 'TV_SHOWS': return mainShowsLocalApi;
  default: return mainMoviesLocalApi;
  }
};

const getLocalRequestConfig = ({
  page, 
  genres, 
  year, 
  certification,
  section
}:GetMainRequestConfig) => {
  switch(section){
  case 'MOVIES': return {
    method: 'get',
    params: {
      ...(year && {year}),
      ...(page && {page}),
      ...(genres && {genres}),
      ...(certification && {certification}),
    },
  };
  case 'TV_SHOWS': return {
    method: 'get',
    params: {
      ...(year && {year}),
      ...(page && {page}),
      ...(genres && {genres})
    },
  };
  default: return {
    method: 'get',
    params: {
      ...(year && {year}),
      ...(page && {page}),
      ...(genres && {genres})
    },
  };
  }
};

/**
 * Get an axios request config to use in main page with movies or shows endpoint
 *
 * @param {GetMainRequestConfig} { 
 *   page, 
 *   genres, 
 *   year, 
 *   certification 
 * }
 * - page: page's movies results
 * - genres: genres's movies results
 * - year: year of movie release
 * - certification: movie's US certification
 * @return {{api, requestConfig}} -
 * - api: Axios instance for "discover movies" endpoint
 * - requestConfig: Axios request config for "discover movies" endpoint
 */
export const getLocalRequestApiConfig = ({ 
  page, 
  genres, 
  year, 
  certification,
  section
}:GetMainRequestConfig) => {
  return {
    api: getLocalApi({section}),
    requestConfig: getLocalRequestConfig({
      page, 
      genres, 
      year, 
      certification,
      section
    })
  };
};

/**
 * Given different responses, the same object is returned
 *
 * @param {{results, section}} 
 * - results: array of results from a response
 * - section: indicates how to normalize a result <"MOVIES" | "TV_SHOWS">
 * @return {{category, id, title, score, img}} 
 */
export const normalizeLocalResponse:NormalizeLocalResponse = ({
  results, section
})=>{
  if(!results) return null;
  switch(section){
  case 'MOVIES': 
    return results.map( result => {
      return {
        category: 'MOVIES',
        id: result.id,
        title: result.title,
        score: result.vote_average ? result.vote_average : 0,
        img: result.poster_path ? `${IMAGE_BASE_URL}${result.poster_path}` : logo
      };
    });
  case 'TV_SHOWS': 
    return results.map( result => {
      return {
        category: 'TV_SHOWS',
        id: result.id,
        title: result.name,
        score: result.vote_average ? result.vote_average : 0,
        img: result.poster_path ? `${IMAGE_BASE_URL}${result.poster_path}` : logo
      };
    });
  default: return null;
  }
};

/**
 * Gets a background classname used to indicate the level score at a card
 *
 * @param {number} value
 * @return {string} className
 */
export const getScoreClassName = (value:number) => {
  if (value >= 7) return GREAT_SCORE_BACKGROUND_CLASS;
  if (value >= 5) return MEDIUM_SCORE_BACKGROUND_CLASS;
  if (value > 0) return LOW_SCORE_BACKGROUND_CLASS;
  return NO_SCORE_BACKGROUND_CLASS;
};

export const normalizeLocalSearchResponse = ({ results }:NormalizeLocalSearchResponseProps) => {
  if(!results) return null;
  const normalizedArray:CardProps[] = [];
  results.forEach( result => {
    switch(result.media_type){
    case 'movie': {
      const normalized = normalizeLocalResponse({
        section: 'MOVIES',
        results: [result]
      });
      if(normalized)
        normalizedArray.push(normalized[0]);
      break;
    }
    case 'tv': {
      const normalized = normalizeLocalResponse({
        section: 'TV_SHOWS',
        results: [result]
      });
      if(normalized)
        normalizedArray.push(normalized[0]);
      break;
    }
    }
    
  });
  return normalizedArray;
};

export const getLocalSearchRequestConfig = ({page, query}:GetLocalSearchRequestConfigProps) => {
  const config = {
    api: searchMoviesTvShowsLocalApi,
    requestConfig: {
      method: 'get',
      params: {
        query: query || 'disney',
        page
      },
    }
  }; 
  
  return config;
};

export const getGenericDetailsRequestConfig = ({ 
  id, baseUrl
}:getGenericDetailsRequestConfigProps) => {
  const { apiKey } = enviromentVariables;
  return {
    api: genericAxiosInstance({ baseUrl }),
    requestConfig: {
      ...(id ? { url: id } : undefined),
      params:{
        api_key: apiKey,
        language: 'en-US'
      }
    }
  };
};

export const normalizeMovieDetails = ({ data }:NormalizeMovieDetailsProps) => {
  return{
    title: data.title,
    score: data.vote_average ? Number(data.vote_average.toFixed(1)) : 0,
    img: data.poster_path ? `${IMAGE_BASE_URL}${data.poster_path}` : logo,
    sectionTitle: 'Overview',
    sectionContent: data.overview || 'No overview is available yet',
    genders: data.genres,
    releaseDate: data.release_date,
    persons: data.persons.cast.map( person => ({
      title: person.name,
      img: person.profile_path ? `${IMAGE_BASE_URL}${person.profile_path}` : logo,
      character: person.character,
      id: person.id,
      link: `${Routes['PERSON']}/${person.id}`,
      official: true
    })),
    crewPersons: data.persons.crew.map( person => ({
      title: person.name,
      img: person.profile_path ? `${IMAGE_BASE_URL}${person.profile_path}` : logo,
      character: person.department,
      id: person.id,
      link: `${Routes['PERSON']}/${person.id}`,
      official: true,
      jobPersonId: person.job ? `${person.id}-${person.job}` : undefined,
      job: person.job ? person.job  : undefined
    })),
  };
};

export const normalizeTvShowDetails = ({ data, showId }:NormalizeTvShowDetailsProps) => {
  return{
    title: data.name,
    score: data.vote_average ? Number(data.vote_average.toFixed(1)) : 0,
    img: data.poster_path ? `${IMAGE_BASE_URL}${data.poster_path}` : logo,
    sectionTitle: 'Overview',
    sectionContent: data.overview || 'No overview is available yet',
    genders: data.genres,
    releaseDate: data.first_air_date,
    persons: data.persons.cast.map( person => ({
      title: person.name,
      img: person.profile_path ? `${IMAGE_BASE_URL}${person.profile_path}` : logo,
      character: person.character,
      id: person.id,
      link: `${Routes['PERSON']}/${person.id}`,
      official: true
    })),
    crewPersons: data.persons.crew.map( person => ({
      title: person.name,
      img: person.profile_path ? `${IMAGE_BASE_URL}${person.profile_path}` : logo,
      character: person.department,
      id: person.id,
      link: `${Routes['PERSON']}/${person.id}`,
      official: true,
      jobPersonId: person.job ? `${person.id}-${person.job}` : undefined,
      job: person.job ? person.job  : undefined
    })),
    seasons: data.seasons.map( season => ({
      title: season.name,
      overview: season.overview,
      id: season.season_number,
      seasonNumber: season.season_number,
      img: season.poster_path ? `${IMAGE_BASE_URL}${season.poster_path}` : logo,
      releaseDate: season.air_date,
      link: `${Routes['SEASON']}/${showId}?seasonNumber=${season.season_number}`,
      official: (season.season_number !== 0 && season.air_date) ? true : false
    }))
  };
};

export const normalizeSeasonDetails = ({ data }:NormalizeSeasonDetailsProps) => {
  return{
    title: data.name,
    img: data.poster_path ? `${IMAGE_BASE_URL}${data.poster_path}` : logo,
    sectionTitle: 'Overview',
    sectionContent: data.overview || 'No overview is available yet',
    releaseDate: data.air_date,
    episodes: data.episodes.map( episode => ({
      title: episode.name,
      img: episode.still_path ? `${IMAGE_BASE_URL}${episode.still_path}` : logo,
      id: episode.id,
      official: true
    }))
  };
};

export const getSeasonDetailsRequestConfig = ({flag, id, seasonNumber}:
  GetSeasonDetailsRequestConfigProps) =>{
  const {seasonDetailsLocalBaseUrl} = enviromentVariables;
  return(
    { 
      flag: flag ? 'FETCH' : 'WAIT' as ('FETCH' | 'WAIT'),
      api: genericAxiosInstance({baseUrl: seasonDetailsLocalBaseUrl || ''}),
      requestConfig:{
        ...(id ? {url: String(id)} : undefined),
        ...(seasonNumber ? {params: { seasonNumber }} : undefined)
      }
    });
};


export const normalizePersonDetails = ({ data }:NormalizePersonDetailsProps) => {
  return{
    title: data.name,
    img: data.profile_path ? `${IMAGE_BASE_URL}${data.profile_path}` : logo,
    sectionTitle: 'Biography',
    sectionContent: data.biography || 'No biography is available yet',
    birthday: data.birthday,
    deathday: data.deathday,
    id: data.id,
    placeOfBirth: data.place_of_birth
  };
};

export const getPersonDetailsRequestConfig = ({flag, id}:
  GetPersonDetailsRequestConfigProps) =>{
  const {personDetailsLocalBaseUrl} = enviromentVariables;
  return(
    { 
      flag: flag ? 'FETCH' : 'WAIT' as ('FETCH' | 'WAIT'),
      api: genericAxiosInstance({baseUrl: personDetailsLocalBaseUrl || ''}),
      requestConfig:{
        ...(id ? {url: String(id)} : undefined)
      }
    });
};

export const getAuthUrl = (requestToken:string) => {
  return `${process.env.
    NEXT_PUBLIC_AUTH_BASE_URL}${requestToken}?redirect_to=${process.env.
    NEXT_PUBLIC_AFTER_AUTH_REDIRECT_BASE_URL}`;
};
