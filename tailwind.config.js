/** @type {import('tailwindcss').Config} */
const plugin = require('tailwindcss/plugin');

module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      keyframes: {
        underline: {
          '0%': { width: '0' },
          '100%': { width: '100%' },
        },
        show: {
          '0%': { opacity: '0' },
          '100%': { opacity: '1' },
        },
        cubeDelay: {
          '0%, 70%, 100%': {
            transform: 'scale3D(1, 1, 1)',
          }, 
          '35%': {
            transform: 'scale3D(0, 0, 1)'
          } 
        }
      },
      animation: {
        'underline': 'underline 0.2s ease-in forwards',
        'show': 'show 0.4s ease-in-out forwards',
        'cube-delay': 'cubeDelay 1.3s infinite ease-in-out'
      },
      'backgroundColor':{
        'dark': '#04060A',
        'dark-glass': 'rgba(10, 10, 10, 0.77)',
        'white-glass': 'rgba(148, 163, 184, 0.2);'
      },
    },
  },
  plugins: [
    require('@tailwindcss/line-clamp'),
    plugin(function({ addUtilities, matchUtilities, theme }) {
      addUtilities({
        '.flex-center': {
          'display': 'flex',
          'justify-content': 'center',
          'align-items': 'center',
          'gap': '1rem',
        },
        '.button': {
          'display': 'flex',
          'justify-content': 'center',
          'align-items': 'center',
          'height': '4rem',
          'min-width': '6rem',
          'background-color': 'transparent',
          'border-radius': '0.375rem',
          'font-size': '1.125rem',
          'cursor': 'pointer',
          'position': 'relative',
          'font-weight': '600',
          'margin-right': '0.5rem',
        },
      }),      
      matchUtilities(
        {
          'bg-gradient': (angle) => ({
            'background-image': `linear-gradient(${angle}, var(--tw-gradient-stops))`,
          }),
        },
        {
          // values from config and defaults you wish to use most
          values: Object.assign(
            theme('bgGradientDeg', {}), // name of config key. Must be unique
            {
              10: '10deg', // bg-gradient-10
              15: '15deg',
              20: '20deg',
              25: '25deg',
              30: '30deg',
              45: '45deg',
              60: '60deg',
              90: '90deg',
              120: '120deg',
              135: '135deg',
            }
          )
        },
      ),      
      matchUtilities(
        {
          'animation-delay': (value) => {
            return {
              'animation-delay': value,
            };
          },
        },
        {
          values: theme('transitionDelay'),
        }
      );
    })
  ],
  safelist: [
    'bg-teal-700',
    'bg-yellow-600',
    'bg-rose-900',
    'bg-black',
    'invert',
    'contrast-100'
  ]
};