import { screen, waitFor } from '@testing-library/dom';
import { act, render } from '@testing-library/react';
import PageProvider from '../testUtils/PageProvider';
import SearchPage from '../pages/search';

jest.mock('next/router', () => ({
  useRouter() {
    return {
      pathname: 'HOME',
    };
  },
}));

describe('Test home page', () => {
  it('Should render movie section & card with data fetched', async () => {
    await act(() => {
      render(
        <PageProvider>
          <SearchPage />
        </PageProvider>
      );
    });
    const sectionTitle = screen.getByText(/movies & tv shows/i);
    expect(sectionTitle).toBeInTheDocument();
    await waitFor(() => {
      const cardTitle = screen.getByText(/insider/i);
      expect(cardTitle).toBeInTheDocument();
    });
  });
});