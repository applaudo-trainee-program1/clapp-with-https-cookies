import { screen, waitFor } from '@testing-library/dom';
import { act, render } from '@testing-library/react';
import PageProvider from '../testUtils/PageProvider';
import SeasonDetails from '../pages/season/[id]';
import seasonDetails from '../mocks/responses/seasonDetails.json';

jest.mock('next/router', () => ({
  useRouter() {
    return {
      pathname: 'HOME',
      query: { id: 70523 },
      isReady: true
    };
  },
}));

describe('Test season details page', () => {
  it('Should render season details section & card with data fetched', async () => {
    await act(() => {
      render(
        <PageProvider>
          <SeasonDetails />
        </PageProvider>
      );
    });
    await waitFor(() => {
      const title = screen.getByText(seasonDetails.name);
      const posterImg = screen.getByAltText(seasonDetails.name);
      expect(title).toBeInTheDocument();
      expect(posterImg).toBeInTheDocument();
    });
  });
});