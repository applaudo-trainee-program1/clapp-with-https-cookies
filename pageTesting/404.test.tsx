import { screen } from '@testing-library/dom';
import { act, render } from '@testing-library/react';
import NotFound from '../pages/404';
describe('Test Header', () => {
  it('Should render an error message', async () => {
    await act(() => {
      render(
        <NotFound />
      );
    });
    const errorMessage = screen.getByText(/resource not found/i);
    expect(errorMessage).toBeInTheDocument();
  });
  
  it('Should render links to home & search sections', async () => {
    await act(() => {
      render(
        <NotFound />
      );
    });
    const homeLink = screen.getByText(/home/i);
    const searchLink = screen.getByText(/search/i);
    expect(homeLink).toBeInTheDocument();
    expect(searchLink).toBeInTheDocument();
  });
});
