import '../styles/globals.css';
import type { AppProps } from 'next/app';
import { store } from '../redux/store';
import { Provider } from 'react-redux';
import { MainLayout } from '../components';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { persistStore } from 'redux-persist';

if (process.env.NEXT_PUBLIC_API_MOCKING === 'true') 
  import('../mocks').then(({ setupMocks }) => {
    setupMocks();
  });

const persistor = persistStore(store);


export default function App({ Component, pageProps }: AppProps) {
  return  (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <MainLayout>
          <Component {...pageProps} />
        </MainLayout>
      </PersistGate>
    </Provider>
  );
}
