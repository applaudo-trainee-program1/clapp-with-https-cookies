import Head from 'next/head';
import ListSection from '../components/ListSection/ListSection';

const Home = () => {
  return (
    <>
      <Head>
        <title>Clapp - Home</title>
        <meta name="description" content="Clapp - home page" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main >
        <ListSection section="MOVIES" sectionTitle='Movies' paginationTitle="Movie"/>
        <ListSection section="TV_SHOWS" sectionTitle='TV shows' paginationTitle="Show"/>
      </main>
    </>
  );
};

export default Home;