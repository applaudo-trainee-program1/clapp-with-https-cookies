import Head from 'next/head';
import { genericApi } from '../apiInstances';
import { GridLayout, Spinner } from '../components';
import { useFetch } from '../hooks';
import { IResponseDiscoverMovies } from '../ts/response';
import { normalizeLocalResponse } from '../utils/utils';


const Profile = () => {
  const api = genericApi({baseUrl: process.env.NEXT_PUBLIC_PROFILE_LOCAL_BASE_URL || ''});
  const {
    loading, 
    response
  } = useFetch<IResponseDiscoverMovies>({ api, 
    requestConfig:{
      withCredentials: true
    }});

  const normalizedResponse = normalizeLocalResponse({
    results: response?.results, 
    section: 'MOVIES'
  });


  return (  
    <>
      <Head>
        <title>Clapp - Home</title>
        <meta name="description" content="Clapp - resource not found" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main >
        {loading === false && normalizedResponse ?
        
          <GridLayout 
            loading={loading} 
            dataArray={normalizedResponse} 
            titleLayout="Favorite movies"
            section = "MOVIES"
          />
          :  
          <div className='flex justify-center items-center min-h-[20rem]'><Spinner/></div>
        }
      </main>
    </>
  );
};
export default Profile;
