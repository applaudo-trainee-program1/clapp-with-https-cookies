import { NextApiRequest, NextApiResponse } from 'next';
import { genericApi } from '../../apiInstances';
import { enviromentVariables } from '../../utils';
import { camelizeKeys, decamelizeKeys } from 'humps';
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import cookie from 'cookie';

const { apiKey } = enviromentVariables;
const api = genericApi({baseUrl: process.env.NEXT_PUBLIC_PROFILE_BASE_URL || ''});
api.defaults.params = {};
api.interceptors.request.use(function (config) {
  config.params['api_key'] = apiKey;
  return config;
}, function (error) {
  return Promise.reject(error);
});


api.interceptors.response.use((response: AxiosResponse) => {
  if (response.data ) 
    response.data = camelizeKeys(response.data);
  return response;
});
    
api.interceptors.request.use((config: AxiosRequestConfig) => {
  const newConfig = { ...config };

  if (config.params) 
    newConfig.params = decamelizeKeys(config.params);
  
  if (config.data) 
    newConfig.data = decamelizeKeys(config.data);
  

  return newConfig;
});

export interface IRequestTokenResponse {
  success: boolean, 
  expiresAt: string, 
  requestToken: string
}

const profile = async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const cookies = cookie.parse(req.headers.cookie || '');
    const account = cookies.sessionId;
    const accountObject = JSON.parse(account);
    const moviesResponse = await api({
      baseURL: process.env.NEXT_PUBLIC_PROFILE_BASE_URL,
      method:'get',
      url: `${accountObject?.accountId}/favorite/movies`,
      params:{
        sessionId: accountObject?.sessionId
      },
    });
    res.status(200).json(moviesResponse.data);
  } catch (error) {
    res.status(400).json({ success: false });
  }
};

export default profile;