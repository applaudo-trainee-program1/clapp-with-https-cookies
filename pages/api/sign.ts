import { NextApiRequest, NextApiResponse } from 'next';
import { genericApi } from '../../apiInstances';
import { camelizeKeys, decamelizeKeys } from 'humps';
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import { enviromentVariables } from '../../utils';

const { apiKey } = enviromentVariables;
const api = genericApi({baseUrl: 'https://api.themoviedb.org/3/authentication/session/new' || ''});
api.defaults.params = {};
api.interceptors.request.use(function (config) {
  config.params['api_key'] = apiKey;
  return config;
}, function (error) {
  return Promise.reject(error);
});


api.interceptors.response.use((response: AxiosResponse) => {
  if (response.data ) 
    response.data = camelizeKeys(response.data);
  return response;
});
    
api.interceptors.request.use((config: AxiosRequestConfig) => {
  const newConfig = { ...config };

  if (config.params) 
    newConfig.params = decamelizeKeys(config.params);
  
  if (config.data) 
    newConfig.data = decamelizeKeys(config.data);
  

  return newConfig;
});

export interface ISessionIdResponse {
  sessionId: string
}

const sign = async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const { requestToken } = req.query;
    const response = await api<ISessionIdResponse>({
      method: 'post',
      data: {
        requestToken: requestToken
      }
    });
    const responseAccount = await api<{username: string, id: number}>({
      method: 'get',
      baseURL: 'https://api.themoviedb.org/3/account',
      params:{
        sessionId: response.data.sessionId
      }
    });
    
    res.status(200).json({ 
      success: true, 
      isLogged: true, 
      sessionId: response.data.sessionId,
      accountName: responseAccount.data.username,
      accountId: responseAccount.data.id
    });
  } catch (error) {
    res.status(400).json({ success: false });
  }
};

export default sign;