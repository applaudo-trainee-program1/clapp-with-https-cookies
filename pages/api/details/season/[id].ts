import { getGenericDetailsRequestConfig } from './../../../../utils/utils';
import { TV_SHOW_DETAILS_BASE_URL } from './../../../../config/env';
import { NextApiRequest, NextApiResponse } from 'next';
import { isValidCastNumber } from '../../../../utils/validations';

const tvShowsDetails = async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const { id, seasonNumber } = req.query;
    const validatedId = isValidCastNumber({toValidate: id});
    const validatedSeasonNumber = isValidCastNumber({toValidate: seasonNumber});
    if(!validatedId) res.status(400).json({message: 'No id where provided'});
    const { requestConfig, api } = getGenericDetailsRequestConfig({
      baseUrl: `${TV_SHOW_DETAILS_BASE_URL}${id}/season/${validatedSeasonNumber}` || ''
    });

    const response = await api({
      ...requestConfig
    }); 
    res.status(200).json(response.data);
  } catch (error) {
    res.status(404).json({message: 'Resource not found'});
  }
};

export default tvShowsDetails;