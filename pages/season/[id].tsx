import Head from 'next/head';
import { useRouter } from 'next/router';
import { useFetch } from '../../hooks';
import {  ISeasonDetails } from '../../ts/response';
import { Spinner, HeaderDetails, DescriptionSection, RelatedSection, 
  ErrorCard} from '../../components';
import { getSeasonDetailsRequestConfig, normalizeSeasonDetails } from '../../utils/utils';


const SeasonDetails = () => {
  const router = useRouter();
  const { id, seasonNumber: season } = router.query;
  const numberId = Number.isNaN(Number(id)) ? 0 : Number(id);
  const seasonNumber = Number.isNaN(Number(season)) ? 0 : Number(season);
  const {
    loading, 
    response,
    error
  } = useFetch<ISeasonDetails>(
    getSeasonDetailsRequestConfig({flag: router.isReady, id:numberId, seasonNumber}));
  const normalizedData = (response && numberId)
    ? normalizeSeasonDetails({data: response}) : null;

  return (
    <>
      <Head>
        <title>Clapp</title>
        <meta name="description" content="Clapp tv show season details" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className='my-4'>
        {loading === false && normalizedData
          ? 
          <>
            <HeaderDetails title={normalizedData.title} image={normalizedData.img} />

            <DescriptionSection title={normalizedData.sectionTitle} 
              description={normalizedData.sectionContent}/>

            <RelatedSection title='Episodes' dataArray={normalizedData.episodes}/>
            
            <span className='text-lg font-bold my-4'>
              Release date: { normalizedData.releaseDate }
            </span>
          </>
          : null
        }
        { loading === true 
          ? 
          <div className='flex justify-center items-center min-h-[4rem] my-16'>
            <Spinner />
          </div>
          : null
        }
        { loading === false && error 
          ? <ErrorCard />
          : null
        }
      </main>
    </>
  );
};
export default SeasonDetails;