import Head from 'next/head';
import { useRouter } from 'next/router';
import movieDetailsLocalApi from '../../apiInstances/movieDetailsLocalApi';
import { useFetch } from '../../hooks';
import { IMovieDetails } from '../../ts/response';
import { Spinner, HeaderDetails, DescriptionSection, TagList, ErrorCard, 
  RelatedSection } from '../../components';
import { normalizeMovieDetails } from '../../utils/utils';

const MovieDetails = () => {
  const router = useRouter();
  const { id } = router.query;
  const {
    loading, 
    response,
    error
  } = useFetch<IMovieDetails>(
    { flag: router.isReady ? 'FETCH' : 'WAIT',
      api: movieDetailsLocalApi,
      requestConfig:{
        ...(id ? {url: String(id)} : undefined) 
      }
    }
  );
  const normalizedData = response ? normalizeMovieDetails({data: response}) : null;
  
  return (
    <>
      <Head>
        <title>Clapp</title>
        <meta name="description" content="Clapp movie details" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className='my-4'>
        {loading === false && normalizedData
          ? 
          <>
            <HeaderDetails title={normalizedData.title} image={normalizedData.img} 
              score={normalizedData.score}/>

            <DescriptionSection title={normalizedData.sectionTitle} 
              description={normalizedData.sectionContent}/>

            <TagList title="Genres" list={normalizedData
              .genders?.map(gender => gender.name)}/>            

            <RelatedSection title='Cast' dataArray={normalizedData.persons}/>

            <RelatedSection title='Crew' dataArray={normalizedData.crewPersons}/>
            <span className='text-lg font-bold my-2'>
              Release date: { normalizedData.releaseDate }
            </span>
          </>
          : null
        }
        
        { loading === true 
          ? 
          <div className='flex justify-center items-center min-h-[4rem] my-16'>
            <Spinner />
          </div>
          : null
        }
        
        { loading === false && error 
          ? <ErrorCard />
          : null
        }
      </main>
    </>
  );
};
export default MovieDetails;