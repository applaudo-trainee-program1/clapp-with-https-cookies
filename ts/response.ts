export interface IMainMovieResult{
  backdrop_path: string | null,
  genre_ids: number[],
  id: number,
  title: string,
  overview: string,
  poster_path: string | null,
  release_date: string,
  vote_average: number
}
export interface IResponseMainMovies {
  page: number,
  total_pages: number,
  total_results: number,
  results: Array<IMainMovieResult>
}
export interface IDiscoverMovieResult{
  backdrop_path: string | null,
  genre_ids: number[],
  id: number,
  title: string,
  overview: string,
  poster_path: string | null,
  release_date: string,
  vote_average: number
  media_type: 'movie'
}

export interface IDiscoverShowResult{
  backdrop_path: string | null,
  genre_ids: number[],
  id: number,
  name: string,
  overview: string,
  poster_path: string | null,
  first_air_date: string,
  vote_average: number
  media_type: 'tv'
}

export interface IResponseDiscover<T> {
  page: number,
  total_pages: number,
  total_results: number,
  results: Array<T>
}

export interface IResponseDiscoverMovies extends IResponseDiscover<IDiscoverMovieResult>{}
export interface IResponseDiscoverShows extends IResponseDiscover<IDiscoverShowResult>{}

export interface IPeopleResult{
  media_type: 'person',
  id: number,
  name: string,
  popularity: number,
  profile_path: string | null
}

export interface IResponsePeople extends IResponseDiscover<IPeopleResult>{}

export interface  IMovieDetails{
  genres: Array<{id: number, name: string}>,
  id: number,
  title: string,
  overview: string,
  poster_path: string | null,
  release_date: string,
  vote_average: number,
  persons: {
    cast: CastPerson[],
    crew: CrewPerson[]
  },
}

export interface CastPerson {
  id: number,
  gender: number,
  name: string,
  profile_path: string,
  character: string
}

export interface CrewPerson {
  id: number,
  gender: number,
  name: string,
  profile_path: string,
  department: string,
  job?: string
}

export interface Season{
  air_date: null | string,
  episode_count: number,
  id: number,
  season_number: number,
  overview: string,
  poster_path: string,
  name: string
}

export interface  ITvShowDetails{
  genres: Array<{id: number, name: string}>,
  id: number,
  name: string,
  overview: string,
  poster_path: string | null,
  first_air_date: string,
  vote_average: number,
  persons: {
    cast: CastPerson[],
    crew: CrewPerson[]
  },
  seasons: Season[]
}

export interface Episode{
  air_date: string,
  episode_number: number,
  id: number,
  name: string,
  overview: string,
  season_number: number,
  vote_average: number,
  still_path: string
  crew: CrewPerson[]
}
export interface  ISeasonDetails{
  air_date: null | string,
  id: number,
  season_number: number,
  overview: string,
  poster_path: string,
  name: string,
  episodes: Episode[]
}

export interface  IPersonDetails{
  biography: string,
  id: number,
  birthday: string | null,
  deathday: string | null,
  profile_path: string,
  name: string,
  place_of_birth: string | null
}

export interface ILocalRequestTokenResponse{
  requestToken: string
}

export interface ILocalSessionResponse {
  sessionId: string, 
  accountId: number, 
  accountName: string
}