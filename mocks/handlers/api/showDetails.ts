import { rest } from 'msw';
import showDetails from '../../responses/showDetails.json';

export const showDetailsHandler = [
  rest.get('/api/details/tv-shows/85937', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(showDetails));
    
  }),
];
