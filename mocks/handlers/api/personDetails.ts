import { rest } from 'msw';
import personDetails from '../../responses/personDetails.json';

export const personDetailsHandler = [
  rest.get('/api/details/person/1256603', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(personDetails));
  }),
];
