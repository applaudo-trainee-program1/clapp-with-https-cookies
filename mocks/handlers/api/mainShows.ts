import { rest } from 'msw';
import mainShows from '../../responses/mainShows.json';

export const mainShowsHandler = [
  rest.get('/api/main/shows', (req, res, ctx) => {
    const page = req.url.searchParams.get('page');
    if(page === '2')  
      return res(ctx.status(400), ctx.json({error: 'resource not found'}));
    
    return res(ctx.status(200), ctx.json(mainShows));
    
  }),
];
