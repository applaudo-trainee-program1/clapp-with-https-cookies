import { mainMoviesHandler } from './api/mainMovies';
import { mainShowsHandler } from './api/mainShows';
import { searchHandler } from './api/search';
import { movieDetailsHandler } from './api/movieDetails';
import { showDetailsHandler } from './api/showDetails';
import { personDetailsHandler } from './api/personDetails';
import { seasonDetailsHandler } from './api/seasonDetails';

export const handlers = [
  ...mainMoviesHandler,
  ...mainShowsHandler,
  ...searchHandler,
  ...movieDetailsHandler,
  ...showDetailsHandler,
  ...personDetailsHandler,
  ...seasonDetailsHandler
];