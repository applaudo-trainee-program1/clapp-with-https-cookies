import { setupWorker } from 'msw';
import { handlers } from './handlers/index';


export const mswWorker = setupWorker(...handlers);