import { getLocalRequestApiConfig } from '../../utils/utils';

export const mockMainMoviesRequestConfig = getLocalRequestApiConfig({page:1, section: 'MOVIES', 
});
export const mockMainMoviesRequestConfigError = getLocalRequestApiConfig({
  page:2, section: 'MOVIES'});