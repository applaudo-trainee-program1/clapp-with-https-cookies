import { enviromentVariables } from '../utils';
import genericAxiosInstance from './genericApi';

const { movieDetailsBaseUrl } = enviromentVariables;

const movieDetailsApi = genericAxiosInstance({
  baseUrl: movieDetailsBaseUrl || ''
});

export default movieDetailsApi;
