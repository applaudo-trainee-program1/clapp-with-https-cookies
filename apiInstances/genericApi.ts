import axios from 'axios';

type GenericAxiosInstanceProps = {
  baseUrl: string
}

const genericAxiosInstance = ({ baseUrl }:GenericAxiosInstanceProps) => {
  const axiosInstance = axios.create({
    baseURL: baseUrl,
    headers: { 'Content-Type': 'application/json; charset=UTF-8' }
  });
  return axiosInstance;
};

export default genericAxiosInstance;
