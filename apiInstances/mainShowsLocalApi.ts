import {enviromentVariables} from '../utils';
import genericAxiosInstance from './genericApi';

const { mainShowsLocalBaseUrl } = enviromentVariables;

const mainShowsLocalApi = genericAxiosInstance({
  baseUrl: mainShowsLocalBaseUrl || ''
});

export default mainShowsLocalApi;
