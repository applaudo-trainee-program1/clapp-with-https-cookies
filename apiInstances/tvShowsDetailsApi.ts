import { enviromentVariables } from '../utils';
import genericAxiosInstance from './genericApi';

const { tvShowsDetailsBaseUrl } = enviromentVariables;

const movieDetailsApi = genericAxiosInstance({
  baseUrl: tvShowsDetailsBaseUrl || ''
});

export default movieDetailsApi;
