
import {enviromentVariables} from '../utils';
import genericAxiosInstance from './genericApi';

const { mainShowsBaseUrl } = enviromentVariables;

const mainShowsApi = genericAxiosInstance({
  baseUrl: mainShowsBaseUrl || ''
});

export default mainShowsApi;
