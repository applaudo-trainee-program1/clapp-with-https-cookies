import { enviromentVariables } from '../utils';
import genericAxiosInstance from './genericApi';

const { movieDetailsLocalBaseUrl } = enviromentVariables;

const movieDetailsLocalApi = genericAxiosInstance({
  baseUrl: movieDetailsLocalBaseUrl || ''
});

export default movieDetailsLocalApi;
