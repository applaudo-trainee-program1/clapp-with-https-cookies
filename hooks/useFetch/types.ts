import { AxiosInstance, AxiosRequestConfig } from 'axios';

export type RequestConfig = {
  method: 'get' | 'post' | 'delete' | 'put' | 'patch',
  params: Record<string, string | number>,
  url?: string
}


export type useFetchParams = {
  api: AxiosInstance
  requestConfig: AxiosRequestConfig<Record<string, string | number | boolean>>,
  flag?: 'FETCH' | 'WAIT'
}
