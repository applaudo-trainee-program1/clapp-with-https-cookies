import { screen, waitFor } from '@testing-library/dom';
import { render } from '@testing-library/react';
import Fetch from '../../testUtils/Fetch';
import mainMovies from '../../mocks/responses/mainMovies.json';
import { mockMainMoviesRequestConfig, 
  mockMainMoviesRequestConfigError } from '../../mocks/requestConfig/mainMovies';

describe('Test hook: useFetch', () => {
  it('response was rendered sucessfully', async () => {
    render(<Fetch config={mockMainMoviesRequestConfig} />);
    const loading = screen.getByLabelText('loading');
    expect(loading.textContent).toBe('loading');
    const response = screen.getByLabelText('response');
    await waitFor(() => {
      expect(loading.textContent).toBe('');
      expect(response.textContent).toEqual(mainMovies.results[0].title);
    });
  });
  it('error was rendered sucessfully', async () => {
    render(<Fetch config={mockMainMoviesRequestConfigError} />);
    const error = screen.getByLabelText('error');
    await waitFor(() => {
      expect(error.textContent).toMatch('400');
    });
  });
});
