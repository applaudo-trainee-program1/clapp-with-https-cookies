export const DEFAULT_PAGE_SIZE = 20;

export const DEFAULT_DEBOUNCE_TIMEOUT = 1000;

export const DEFAULT_TOAST_TIMEOUT = 3000;

export const MAX_TMDB_PAGE_ALLOWED = 500;

export const MIN_PAGE_REQUEST = 1;

export const IMAGE_BASE_URL = 'https://image.tmdb.org/t/p/original';

