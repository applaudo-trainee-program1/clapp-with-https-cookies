import { AxiosInstance, AxiosRequestConfig } from 'axios';
import { useFetch } from '../hooks';
import { IResponseDiscoverMovies } from '../ts/response';

export interface MockResponse{
  data: string
}
export type FetchProps = {
  config:  {
    api: AxiosInstance,
    requestConfig: AxiosRequestConfig
  }
}

const Fetch = ({ config }:FetchProps) => {
  const { error, loading, response } = useFetch<IResponseDiscoverMovies>(config);
  return (
    <div>
      <span aria-label='loading'>{loading ? 'loading' : null}</span>
      <span aria-label='response'>{response ? response.results[0].title : null}</span>
      <span aria-label='error'>{error}</span>
    </div>
  );
};
export default Fetch;
