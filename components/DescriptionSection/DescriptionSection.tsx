import { DescriptionSectionProps } from './types';

const DescriptionSection = ({title, description}:DescriptionSectionProps) => {
  return (
    <section className="flex flex-col justify-center items-center my-4">
      <h4 className='self-start text-lg font-bold'>{ title }</h4>
      <article className="tracking-widestw">
        {description ? description : `No ${title} is available`}
      </article>
    </section>
  );
};
export default DescriptionSection;