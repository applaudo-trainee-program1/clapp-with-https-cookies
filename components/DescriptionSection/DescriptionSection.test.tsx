import { screen } from '@testing-library/dom';
import { render } from '@testing-library/react';
import DescriptionSection from './DescriptionSection';
import props from '../../mocks/props/descriptionSection.json';

describe('Test DescriptionSection', () => {
  it('Should render title & description', async () => {
    render(
      <DescriptionSection
        title={props.title}
        description={props.description}
      />
    );
    const title = screen.getByText(props.title);
    expect(title).toBeInTheDocument();
  });
});
