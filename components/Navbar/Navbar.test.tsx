import { screen } from '@testing-library/dom';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';
import Navbar from './Navbar';

jest.mock('next/router', () => ({
  useRouter() {
    return {
      pathname: 'HOME',
    };
  },
}));

describe('Test MainLayout', () => {
  it('Should render a children', async () => {
    render(
      <Provider store={store}>
        <Navbar />
      </Provider>
    );
    const homeNavItem = screen.getByText('Home');
    const searchNavItem = screen.getByText('Search');
    expect(homeNavItem).toBeInTheDocument();
    expect(searchNavItem).toBeInTheDocument();
  });
});
