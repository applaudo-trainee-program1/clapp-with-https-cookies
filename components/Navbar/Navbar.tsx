import Link from 'next/link';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { RootState } from '../../redux/store';
import { Routes } from '../../routes/routes';
import { NavbarProps } from './types';

const Navbar = ({ handleCloseMenu }: NavbarProps) => {
  const router = useRouter();
  const signState = useSelector((state: RootState) => state.sign);

  const closeMenu = () => {
    if (handleCloseMenu) handleCloseMenu();
  };
  return (
    <nav className='flex justify-start items-end gap-4 flex-col backdrop-blur-sm 
    bg-dark-glass w-full px-4 h-full relative z-40 sm:flex sm:flex-row sm:items-center 
    sm:bg-transparent sm:relative sm:p-0 sm:inset-auto'>
      <Link
        className={router.pathname === Routes.HOME 
          ? `text-pink-600 button after:absolute after:bg-pink-600 after:h-[0.2rem] 
          after:w-0 after:bottom-2 after:animate-underline sm:font-medium`
          : 'sm:font-medium button'}
        onClick={closeMenu}
        href={Routes.HOME}>Home
      </Link>
      <Link 
        className={router.pathname === Routes.SEARCH 
          ? `text-pink-600 button after:absolute after:bg-pink-600 after:h-[0.2rem] 
          after:w-0 after:bottom-2 after:animate-underline sm:font-medium`
          : 'sm:font-medium button'}
        onClick={closeMenu}
        href={Routes.SEARCH}>Search
      </Link>
      {
        signState.isLogged ?
          <Link 
            className={router.pathname === Routes.PROFILE 
              ? `text-pink-600 button after:absolute after:bg-pink-600 after:h-[0.2rem] 
        after:w-0 after:bottom-2 after:animate-underline sm:font-medium`
              : 'sm:font-medium button'}
            onClick={closeMenu}
            href={Routes.PROFILE}>Profile
          </Link> 
          : 
          <Link 
            className={router.pathname === Routes.SIGN 
              ? `text-pink-600 button after:absolute after:bg-pink-600 after:h-[0.2rem] 
      after:w-0 after:bottom-2 after:animate-underline sm:font-medium`
              : 'sm:font-medium button'}
            onClick={closeMenu}
            href={Routes.SIGN}>Sign in
          </Link> 
      }
    </nav>
  );
};

export default Navbar;
