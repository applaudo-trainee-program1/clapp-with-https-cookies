export type TagListProps = {
  title: string,
  list?: string[]
}
const TagList = ({ title, list }:TagListProps) => {
  return (
    <div className="w-full my-4">
      <h4 className='self-start text-lg font-bold'>{ title }</h4>
      <ul className="flex justify-start items-center gap-2">
        {
          list
            ? list.map( item => <li className="bg-white-glass p-2 rounded-md" key={item}>
              { item }
            </li>)
            : `There is no ${title}`
        }
      </ul>
    </div>
  );
};
export default TagList;