import { useDispatch } from 'react-redux';
import { setCertification, setGenres, setYear } from '../../redux/slices/filter/filterSlice';
import { MOVIES_CERTIFICATION_ARRAY, MOVIES_GENRES_ARRAY, 
  TV_SHOWS_GENRES_ARRAY } from '../../utils/constants';
import { CustomSelect, InputDebounce } from '../';
import { FilterGroupProps } from './types';

const FilterGroup = ({section}:FilterGroupProps) => {
  const dispatch = useDispatch();
  
  const updateMovieYear = (value:string) => {
    dispatch(setYear({
      year: Number(value), 
      section: 'MOVIES'}));
  };

  const updateMovieGenres = (values:number[]) => {
    dispatch(setGenres({
      genres: (values as number[]), 
      section: 'MOVIES'}));
  };

  const updateMovieCertification = (values:string[]) => {
    dispatch(setCertification({
      certification: (values), 
      section: 'MOVIES'}));
  };

  const updateShowYear = (value:string) => {
    dispatch(setYear({
      year: Number(value), 
      section: 'TV_SHOWS'}));
  };

  const updateShowGenres = (values:number[]) => {
    dispatch(setGenres({
      genres: (values as number[]), 
      section: 'TV_SHOWS'}));
  };
  const moviesGroup = () => {
    return (
      <>
        <InputDebounce type="INTEGER_RANGE" title='Year' initialValue="" max={2023} min={1980} 
          handleUpdate={(value)  => updateMovieYear(value)} />
        <div className='flex justify-between gap-2 w-full sm:justify-end'>
          <CustomSelect 
            title="Genres" 
            selectionMode = "MULTIPLE"
            optionsArray={MOVIES_GENRES_ARRAY} 
            handleUpdate={(values) => updateMovieGenres(values as number[]) }/>
          <CustomSelect 
            title="Certification" 
            selectionMode = "SINGLE"
            optionsArray={MOVIES_CERTIFICATION_ARRAY} 
            handleUpdate={(values) => updateMovieCertification(values as string[]) }/>
        </div>
      </>
    );
  };
  const tvShowsGroup = () => {
    return (
      <>
        <InputDebounce type="INTEGER_RANGE" title='Year' initialValue="" max={2023} min={1980} 
          handleUpdate={(value) => updateShowYear(value)} />
        <div className='flex justify-between gap-2 w-full sm:justify-end'>
          <CustomSelect 
            title="Genres" 
            selectionMode = "MULTIPLE"
            optionsArray={TV_SHOWS_GENRES_ARRAY} 
            handleUpdate={(values) => updateShowGenres(values as number[]) }/>
        </div>
      </>
    );
  };
  return (
    <div className='flex flex-col items-end justify-center gap-4'> 
      { section==='MOVIES' ? moviesGroup() : tvShowsGroup() }
    </div>
  );
};
export default FilterGroup;
