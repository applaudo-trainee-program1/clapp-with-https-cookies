import { screen } from '@testing-library/dom';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from '../../redux/store';
import FilterGroup from './FilterGroup';

describe('Test custom filter group', () => {
  it('Should render filter related with movies', async () => {
    render(
      <Provider store={store}>
        <FilterGroup section='MOVIES'/>
      </Provider>
    );
    const genres = screen.getByText(/genres/i);
    const certification = screen.getByText(/certification/i);
    const year = screen.getByLabelText(/year/i);
    expect(genres).toBeInTheDocument();
    expect(certification).toBeInTheDocument();
    expect(year).toBeInTheDocument();
  });
  
  it('Should render filter related with tv shows', async () => {
    render(
      <Provider store={store}>
        <FilterGroup
          section='TV_SHOWS'
        />
      </Provider>
    );
    const genres = screen.getByText(/genres/i);
    const certification = screen.queryByText(/certification/i);
    const year = screen.getByLabelText(/year/i);
    expect(genres).toBeInTheDocument();
    expect(certification).not.toBeInTheDocument();
    expect(year).toBeInTheDocument();
  });
});
