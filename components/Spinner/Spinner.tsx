const Spinner = () => {
  return (
    <div className="h-10 w-10" aria-label='spinner loading'>

      <div 
        className={'w-1/3 h-1/3 bg-cyan-50 float-left animate-cube-delay animation-delay-[0.2s]'}
      >
      </div>
      <div 
        className={'w-1/3 h-1/3 bg-cyan-50 float-left animate-cube-delay animation-delay-[0.3s]'}
      >
      </div>
      <div 
        className={'w-1/3 h-1/3 bg-cyan-50 float-left animate-cube-delay animation-delay-[0.4s]'}
      >
      </div>

      <div 
        className={'w-1/3 h-1/3 bg-cyan-50 float-left animate-cube-delay animation-delay-[0.1s]'}
      >
      </div>
      <div 
        className={'w-1/3 h-1/3 bg-cyan-50 float-left animate-cube-delay animation-delay-[0.2s]'}
      >
      </div>
      <div 
        className={'w-1/3 h-1/3 bg-cyan-50 float-left animate-cube-delay animation-delay-[0.3s]'}
      >
      </div>

      <div 
        className={'w-1/3 h-1/3 bg-cyan-50 float-left animate-cube-delay animation-delay-[0s]'}
      >
      </div>
      <div 
        className={'w-1/3 h-1/3 bg-cyan-50 float-left animate-cube-delay animation-delay-[0.1s]'}
      >
      </div>
      <div 
        className={'w-1/3 h-1/3 bg-cyan-50 float-left animate-cube-delay animation-delay-[0.2s]'}
      >
      </div>

    </div>
  );
};
export default Spinner;
