import { screen } from '@testing-library/dom';
import { render } from '@testing-library/react';
import props from '../../mocks/props/HeaderDetails.json';
import HeaderDetails from './HeaderDetails';

jest.mock('next/router', () => ({
  useRouter() {
    return {
      pathname: 'HOME',
      router: () => null
    };
  },
}));
describe('Test HeaderDetails', () => {
  it('Should render an image & title', async () => {
    render(
      <HeaderDetails
        image = {props.image}
        title = {props.title}
        score = {props.score}
      />
    );
    const img = screen.getByAltText(props.title);
    const title = screen.getByText(props.title);
    expect(img).toBeInTheDocument();
    expect(title).toBeInTheDocument();
  });
});
