export type HeaderDetailsProps = {
  title: string,
  image: string,
  score?: number
};