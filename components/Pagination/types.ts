/* eslint-disable no-unused-vars */
export type PaginationProps = {
  page: number,
  resultCount: number,
  setPage: (newPage: number)=>void,
  paginationTitle?: string
}
