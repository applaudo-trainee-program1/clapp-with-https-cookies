import { screen, waitFor } from '@testing-library/dom';
import { render } from '@testing-library/react';
import Pagination from './Pagination';
import props from '../../mocks/props/Pagination.json';
import { MAX_TMDB_PAGE_ALLOWED } from '../../config';
import { calculateTotalPages } from '../../utils/utils';
import userEvent from '@testing-library/user-event';

describe('Test Pagination', () => {
  it('Should render min page', async () => {
    const callbackTest = jest.fn();
    render(
      <Pagination 
        page = {props.page}
        resultCount = {props.resultCount}
        paginationTitle = {props.paginationTitle}
        setPage = {callbackTest}
      />
    );
    const minPage = screen.getByText('1');
    expect(minPage).toBeInTheDocument();
  });
  it('Should render max page', async () => {
    const callbackTest = jest.fn();
    render(
      <Pagination 
        page = {props.page}
        resultCount = {props.resultCount}
        paginationTitle = {props.paginationTitle}
        setPage = {callbackTest}
      />
    );
    const calculatedMaxPage = calculateTotalPages(props.resultCount);
    const calculateLimitedMaxPage = calculatedMaxPage>MAX_TMDB_PAGE_ALLOWED 
      ? MAX_TMDB_PAGE_ALLOWED 
      : calculatedMaxPage;
    const maxPage = screen.getByText(String(calculateLimitedMaxPage));
    expect(maxPage).toBeInTheDocument();
  });
  it('Should render an input', async () => {
    const callbackTest = jest.fn();
    render(
      <Pagination 
        page = {props.page}
        resultCount = {props.resultCount}
        paginationTitle = {props.paginationTitle}
        setPage = {callbackTest}
      />
    );
    const input = screen.getByPlaceholderText(/example/i);
    expect(input).toBeInTheDocument();
  });

  it('Should change page to page 100 when type 100', async () => {
    const callbackTest = jest.fn();
    render(
      <Pagination 
        page = {props.page}
        resultCount = {props.resultCount}
        paginationTitle = {props.paginationTitle}
        setPage = {callbackTest}
      />
    );
    const input = screen.getByLabelText('set input page');
    userEvent.type(input, '100');
    await waitFor(()=>{
      const currentPage = screen.getByLabelText(/current/i);
      expect(currentPage).toHaveTextContent('100');
    });
  });

  
  it('Should change page to page 105, after 5 clicks on next page', async () => {
    const callbackTest = jest.fn();
    render(
      <Pagination 
        page = {100}
        resultCount = {props.resultCount}
        paginationTitle = {props.paginationTitle}
        setPage = {callbackTest}
      />
    );
    const nextButton = screen.getByLabelText(/next/i);
    userEvent.click(nextButton);
    userEvent.click(nextButton);
    userEvent.click(nextButton);
    userEvent.click(nextButton);
    userEvent.click(nextButton);
    await waitFor(()=>{
      const currentPage = screen.getByLabelText(/current/i);
      expect(currentPage).toHaveTextContent('105');
    });
  });
  
  it('Should change page to page 95, after 5 click to previous', async () => {
    const callbackTest = jest.fn();
    render(
      <Pagination 
        page = {100}
        resultCount = {props.resultCount}
        paginationTitle = {props.paginationTitle}
        setPage = {callbackTest}
      />
    );
    const previousButton = screen.getByLabelText(/previous/i);
    userEvent.click(previousButton);
    userEvent.click(previousButton);
    userEvent.click(previousButton);
    userEvent.click(previousButton);
    userEvent.click(previousButton);
    await waitFor(()=>{
      const currentPage = screen.getByLabelText(/current/i);
      expect(currentPage).toHaveTextContent('95');
    });
  });

  it('Should change page to page 99 when select page 99', async () => {
    const callbackTest = jest.fn();
    render(
      <Pagination 
        page = {100}
        resultCount = {props.resultCount}
        paginationTitle = {props.paginationTitle}
        setPage = {callbackTest}
      />
    );
    const selectedPage = screen.getByText(/99/i);
    userEvent.click(selectedPage);
    await waitFor(()=>{
      const currentPage = screen.getByLabelText(/current/i);
      expect(currentPage).toHaveTextContent('99');
    });
  });
  it('Should change page to page 98 when select page 98', async () => {
    const callbackTest = jest.fn();
    render(
      <Pagination 
        page = {100}
        resultCount = {props.resultCount}
        paginationTitle = {props.paginationTitle}
        setPage = {callbackTest}
      />
    );
    const selectedPage = screen.getByText(/98/i);
    userEvent.click(selectedPage);
    await waitFor(()=>{
      const currentPage = screen.getByLabelText(/current/i);
      expect(currentPage).toHaveTextContent('98');
    });
  });
  it('Should change page to page 101 when select page 101', async () => {
    const callbackTest = jest.fn();
    render(
      <Pagination 
        page = {100}
        resultCount = {props.resultCount}
        paginationTitle = {props.paginationTitle}
        setPage = {callbackTest}
      />
    );
    const selectedPage = screen.getByText(/101/i);
    userEvent.click(selectedPage);
    await waitFor(()=>{
      const currentPage = screen.getByLabelText(/current/i);
      expect(currentPage).toHaveTextContent('101');
    });
  });
  it('Should change page to page 102 when select page 102', async () => {
    const callbackTest = jest.fn();
    render(
      <Pagination 
        page = {100}
        resultCount = {props.resultCount}
        paginationTitle = {props.paginationTitle}
        setPage = {callbackTest}
      />
    );
    const selectedPage = screen.getByText(/102/i);
    userEvent.click(selectedPage);
    await waitFor(()=>{
      const currentPage = screen.getByLabelText(/current/i);
      expect(currentPage).toHaveTextContent('102');
    });
  });
});
