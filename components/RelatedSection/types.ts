export type RelatedDataSection = {
  title: string;
  id: number;
  img: string;
  link?: string;
  official: boolean;
  jobPersonId?: string,
  job?: string
}

export type RelatedSectionProps = {
  dataArray: RelatedDataSection[],
  title: string
}