import Image from 'next/image';
import Link from 'next/link';
import { RelatedSectionProps } from './types';
import { motion } from 'framer-motion';
import { containerOpacityVariant, itemOpacityVariant } from '../../utils/constants';

const RelatedSection = ({ dataArray, title }: RelatedSectionProps) => {
  return (
    <section className="flex flex-col justify-center items-start flex-wrap my-4 gap-2" >
      <h4 className='self-start text-lg font-bold'>{title}</h4>
      <motion.div className='grid grid-cols-2 sm:grid-cols-3 lg:grid-cols-4 gap-4' 
        variants={containerOpacityVariant} initial="hidden" animate="show"
      >
        { dataArray.map( data => 
          data.official
            ? data?.link ?
              <Link className='bg-white-glass p-2 rounded-md max-w-[16rem] min-h-[15rem]' 
                key={data.jobPersonId ? data.jobPersonId : data.id} href={data.link}>
                <motion.article className='flex flex-col justify-between h-full' 
                  variants={itemOpacityVariant}>
                  <Image className='w-auto' src={data.img} alt={data.title} height={250} 
                    width={250} />
                  <div>
                    <h6>{data.title}</h6>
                    {data.job ? <span>{data.job}</span> : null}
                  </div>
                </motion.article>
              </Link>
              :
              <motion.article className={`flex flex-col justify-between h-full bg-white-glass p-2 
              rounded-md`} variants={itemOpacityVariant} 
              key={data.jobPersonId ? data.jobPersonId : data.id}>
                <Image className='w-auto' src={data.img} alt={data.title} height={250} 
                  width={250} />
                <div>
                  <h6>{data.title}</h6>
                  {data.job ? <span>{data.job}</span> : null}
                </div>
              </motion.article>
            : null
        ) 
        }
        {!dataArray.length ? 'No data available' : null}
      </motion.div>
    </section>
  );
};
export default RelatedSection;