import { motion } from 'framer-motion';
import { Card, ErrorCard, FilterGroup, Spinner } from '../';
import { containerOpacityVariant, itemOpacityVariant} from '../../utils/constants';
import { GridLayoutProps } from './types';

const GridLayout = ({ loading, dataArray, titleLayout, section }:GridLayoutProps) => {
  return (
    <div className='flex flex-col justify-center my-16'>
      <h2 className='text-2xl text-pink-600 font-bold mb-4'>{titleLayout}</h2>
      {section !== 'BOTH' ? <FilterGroup section={section}/> : null}
      {
        loading 
          ? 
          <div className='flex justify-center items-center min-h-[20rem]'><Spinner/></div>
          : 
          <motion.section className="grid grid-cols-2 sm:grid-cols-3 lg:grid-cols-4 gap-4" 
            variants={containerOpacityVariant}
            initial="hidden"
            animate="show"
          >
            {loading === false && dataArray?.map(({id, title, img, score, category}) => 
              <Card key={id} 
                title={title} 
                img={img} 
                category={category}
                id={id} 
                score={score} 
                itemVariant={itemOpacityVariant}  
              />
            )}
          </motion.section>
      }
      { loading === false && !dataArray?.length 
        ? <ErrorCard showLinks={false}/>
        : null
      }
    </div>
  );
};
export default GridLayout;