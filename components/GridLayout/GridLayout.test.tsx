import { screen } from '@testing-library/dom';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import props from '../../mocks/props/GridLayoutProps.json';
import { store } from '../../redux/store';
import { CardProps } from '../Card/types';
import GridLayout from './GridLayout';
describe('Test GridLayout', () => {
  it('Should render an spinner', async () => {
    render(
      <Provider store={store}>
        <GridLayout
          dataArray={props.dataArray as CardProps[]}
          loading={true}
          section="MOVIES"
          titleLayout='Movies'
        />
      </Provider>
    );
    const spinner = screen.getByLabelText(/loading/i);
    expect(spinner).toBeInTheDocument();
  });
  it('Should render error message when data array is empty', async () => {
    render(
      <Provider store={store}>
        <GridLayout
          dataArray={[]}
          loading={false}
          section="MOVIES"
          titleLayout='Movies'
        />
      </Provider>
    );
    const errorMessage = screen.getByText(/not found/i);
    expect(errorMessage).toBeInTheDocument();
  });

  it('Should render a card when data array is not empty', async () => {
    render(
      <Provider store={store}>
        <GridLayout
          dataArray={props.dataArray as CardProps[]}
          loading={false}
          section="MOVIES"
          titleLayout='Movies'
        />
      </Provider>
    );
    const cardTitle = screen.getByText(props.dataArray[0].title);
    expect(cardTitle).toBeInTheDocument();
  });
});
