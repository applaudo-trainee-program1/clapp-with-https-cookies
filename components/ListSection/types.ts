import { Routes } from '../../routes/routes';

export type ListSectionProps = {
  section: Extract< keyof typeof Routes, 'MOVIES' | 'TV_SHOWS'> ,
  sectionTitle: string,
  paginationTitle: string
}