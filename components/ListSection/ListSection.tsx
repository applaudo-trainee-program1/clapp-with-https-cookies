import {useRef, useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { GridLayout, Pagination } from '../';
import { useFetch } from '../../hooks';
import { RootState } from '../../redux/store';
import { IResponseDiscoverMovies, IResponseDiscoverShows } from '../../ts/response';
import { getLocalRequestApiConfig, normalizeLocalResponse } from '../../utils/utils';
import { ListSectionProps } from './types';

const ListSection = ({ section, sectionTitle, paginationTitle }:ListSectionProps) => {
  const [page, setPage] = useState(1);
  const scrollRef = useRef<HTMLDivElement>(null);
  const filterState = useSelector((state: RootState) => state.filter);
  
  const { api, requestConfig } = getLocalRequestApiConfig({page, section, 
    genres:filterState[section].genres?.join(','),
    certification: filterState[section].certification?.toString(),
    year: Number(filterState[section].year)   
  });
  
  const {
    loading, 
    response, 
    refetch
  } = useFetch<IResponseDiscoverMovies & IResponseDiscoverShows>({ api, requestConfig });

  const normalizedResponse = normalizeLocalResponse({
    results: response?.results, 
    section
  });

  const scrollTo = () => {
    if (scrollRef && scrollRef.current ) 
      scrollRef.current.scrollIntoView({ behavior: 'smooth', block: 'start' });
    
  };

  const handleSetNewPage = (newPage:number) => {
    setPage(newPage);
    refetch();
    scrollTo();
  };

  useEffect(()=>{
    setPage(1);
    refetch();
  },[filterState[section]]);

  return (
    <><div ref={scrollRef}></div>
      <GridLayout 
        loading={loading} 
        dataArray={normalizedResponse} 
        titleLayout={sectionTitle}
        section = {section}
      />
      {loading === false && response && normalizedResponse?.length
        ? <Pagination page={page} setPage={handleSetNewPage}
          resultCount={response.total_results} paginationTitle={paginationTitle}/>
        : null
      }
    </>
  );
};

export default ListSection;