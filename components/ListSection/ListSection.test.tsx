import { screen, waitFor } from '@testing-library/dom';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import props from '../../mocks/props/ListSection.json';
import mainMovieResponseMock from '../../mocks/responses/mainMovies.json';
import { store } from '../../redux/store';
import ListSection from './ListSection';

jest.mock('next/router', () => ({
  useRouter() {
    return {
      pathname: 'HOME',
      router: () => null
    };
  },
}));
describe('Test ListSection', () => {
  it('Should render a section title & a pagination title', async () => {
    render(
      <Provider store={store}>
        <ListSection
          paginationTitle = {props.paginationTitle}
          section = "MOVIES"
          sectionTitle = {props.sectionTitle}
        />
      </Provider>
    );
    await waitFor(async()=>{      
      const title = await screen.findAllByText(/movie/i);
      expect(title).toHaveLength(2);
    });
  });
  
  it('Should render a card', async () => {
    render(
      <Provider store={store}>
        <ListSection
          paginationTitle = {props.paginationTitle}
          section = "MOVIES"
          sectionTitle = {props.sectionTitle}
        />
      </Provider>
    );
    await waitFor(async()=>{      
      const title = await screen.getByAltText(mainMovieResponseMock.results[0].title);
      expect(title).toBeInTheDocument();
    });
  });
});
