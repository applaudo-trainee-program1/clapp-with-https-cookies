import Image from 'next/image';
import Link from 'next/link';
import { errorImg } from '../../public/images';
import { Routes } from '../../routes/routes';
import { ErrorCardProps } from './types';

const ErrorCard = ({showLinks = true}:ErrorCardProps) => {
  return (
    <div className='flex flex-col justify-center items-center gap-4 my-16'>
      <h3 className='font-semibold text-2xl'>Resource not found</h3>
      <Image className='invert' src={errorImg} alt="An error has ocurred" 
        width={200} height={200}/>
      {
        showLinks 
          ? 
          <>
            <Link className='bg-white-glass p-2 rounded-md' href={Routes.HOME}> Go home</Link>
            <Link className='bg-white-glass p-2 rounded-md' href={Routes.SEARCH}>
                Search movies & tv shows
            </Link>
          </>
          : null
      }
    </div>
  );
};
export default ErrorCard;